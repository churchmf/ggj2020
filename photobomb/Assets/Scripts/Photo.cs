﻿using UnityEngine;

[
    RequireComponent(typeof(MeshRenderer))
]
public class Photo : MonoBehaviour
{
    [SerializeField]
    bool _scaleToImageAspectRatio = true;
    [SerializeField]
    bool _snapped = false;

    MeshRenderer _meshRenderer = null;
    Sprite _image = null;

    public void Initialize(Sprite image)
    {
        _image = image;
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.material.SetTexture("_MainTex", image.texture);

        if (_scaleToImageAspectRatio)
        {
            float aspectRatio = image.bounds.size.x / image.bounds.size.y;
            transform.localScale = new Vector3(transform.localScale.x * aspectRatio, transform.localScale.y, transform.localScale.z);
        }
    }

    public Sprite GetImage() { return _image; }

    public void SetSnapped(bool snapped)
    {
        _snapped = snapped;
    }

    public bool IsSnapped()
    {
        return _snapped;
    }
}
