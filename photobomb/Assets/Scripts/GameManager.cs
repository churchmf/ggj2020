﻿using MeshSplitting.Splitters;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class GameManager : MonoBehaviour
{
    [SerializeField]
    Photo _photoPrefab = null;

    [SerializeField]
    Photo _photoFinishedPrefab = null;

    [SerializeField]
    CanvasGroup _canvasGroup = null;

    [SerializeField]
    Transform _finishedSpawn = null;

    [SerializeField]
    Transform _finishedTarget = null;

    [SerializeField]
    Transform _ghost = null;

    [SerializeField]
    Transform _photoSpawn = null;

    [SerializeField]
    Sprite[] _images = null;
    int _imageIndex = 0;

    [SerializeField]
    float _fadeSpeed = 1f;

    Photo[] _pieces = null;
    AudioSource _audioSource = null;

    Coroutine _destroyingPieces = null;
    Coroutine _finished = null;

    void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    void Update()
    {
        if (_pieces != null)
        {
            bool allSnapped = true;
            foreach (Photo piece in _pieces)
            {
                allSnapped &= piece.IsSnapped();
            }

            if (allSnapped && _destroyingPieces == null)
            {
                _imageIndex++;
                _audioSource.Play();

                _destroyingPieces = StartCoroutine(FadePhotoAndDestroySequence(_fadeSpeed));

                Photo finished = Instantiate(_photoFinishedPrefab, _finishedSpawn.position + new Vector3(_imageIndex * 0.5f, _imageIndex * 0.25f, 0f), _finishedSpawn.rotation);
                finished.Initialize(_images[_imageIndex - 1]);
                StartCoroutine(FadeIn(finished.GetComponent<MeshRenderer>(), _fadeSpeed));
            }
        }

        if (_imageIndex >= _images.Length && _finished == null)
        {
            _finished = StartCoroutine(EndSequence(2f, 5f));
        }
    }

    IEnumerator EndSequence(float speed, float distance)
    {
        _ghost.gameObject.SetActive(false);
        var finished = FindObjectsOfType<Photo>().Where(p => p.GetComponent<Rigidbody>().isKinematic).OrderByDescending(p => p.transform.position.y);
        foreach (var photo in finished.Select(p => p.GetComponent<Rigidbody>()))
        {
            photo.isKinematic = false;
            photo.GetComponent<Collider>().enabled = false;
            while (Vector3.Distance(photo.position, _finishedTarget.position) > distance)
            {
                var direction = _finishedTarget.position - photo.position;
                photo.AddForce(direction.normalized * speed, ForceMode.Force);
                yield return null;
            }
            yield return null;
        }

        _canvasGroup.alpha = 0f;
        _canvasGroup.gameObject.SetActive(true);

        do
        {
            _canvasGroup.alpha += Time.deltaTime * 0.25f;
            yield return null;
        }
        while (!Mathf.Approximately(_canvasGroup.alpha, 1f));

        yield return new WaitForSeconds(10f);

        SceneManager.LoadScene("menu");
    }

    IEnumerator FadeIn(MeshRenderer mesh, float speed)
    {
        float alpha = 0f;
        do
        {
            Color color = mesh.material.color;
            alpha += Time.deltaTime * speed;
            color.a = alpha;
            mesh.material.color = color;
            yield return null;
        }
        while (alpha <= 1f);
    }

    IEnumerator FadePhotoAndDestroySequence(float speed)
    {
        float alpha = 1f;
        var meshRenderers = _pieces.Select(p => p.GetComponent<MeshRenderer>());

        do
        {
            foreach (var piece in meshRenderers)
            {
                Color color = piece.material.color;
                alpha -= Time.deltaTime * speed;
                color.a = alpha;
                piece.material.color = color;
            }
            yield return null;
        }
        while (alpha >= 0f);

        foreach (var piece in meshRenderers)
        {
            Destroy(piece.gameObject);
        }

        _pieces = null;
        _destroyingPieces = null;
    }

    public IEnumerator SpawnPhoto()
    {
        if (_imageIndex >= _images.Length)
        {
            Debug.LogError("Finished");
            yield break;
        }

        if (_images == null || _images.Length <= 0)
        {
            Debug.LogError("No images!");
            yield break;
        }

        if (_pieces != null && _destroyingPieces == null)
        {
            _destroyingPieces = StartCoroutine(FadePhotoAndDestroySequence(_fadeSpeed));
        }
        while (_destroyingPieces != null)
        {
            yield return null;
        }

        Photo spawned = Instantiate(_photoPrefab, _photoSpawn.position, _photoSpawn.rotation);
        spawned.Initialize(_images[_imageIndex]);

        _ghost.gameObject.SetActive(true);
        _ghost.localScale = spawned.transform.localScale;

        int numCuts = Mathf.Min(_imageIndex + 1, 4);
        for (int i = 0; i < numCuts; ++i)
        {
            RandomlyCut(spawned.transform);
            yield return null;
        }

        _pieces = FindObjectsOfType<Photo>().Where(p => !p.GetComponent<Rigidbody>().isKinematic).ToArray();
        Shuffle(_pieces.Select(t => t.GetComponent<Rigidbody>()));
    }

    void Shuffle(IEnumerable<Rigidbody> pieces)
    {
        float height = 0f;
        foreach (Rigidbody piece in pieces)
        {
            Vector3 euler = piece.rotation.eulerAngles;
            euler.y = Random.Range(0f, 360f);
            piece.transform.rotation = Quaternion.Euler(euler);

            Vector3 random = Random.onUnitSphere * 2f;
            piece.transform.position = new Vector3(random.x, _photoSpawn.position.y + height, random.z);
            height += 0.5f;
        }
    }

    void RandomlyCut(Transform target)
    {
        Bounds bounds = target.GetComponent<Collider>().bounds;
        float smallest = Mathf.Min(bounds.extents.x, bounds.extents.z);
        Vector2 insideUnitCircle = Random.insideUnitCircle * smallest / 2;
        Cut(target, new Vector3(insideUnitCircle.x, 0f, insideUnitCircle.y), Random.rotation);
    }

    void Cut(Transform target, Vector3 offset, Quaternion rotation)
    {
        GameObject goCutPlane = new GameObject("CutPlane", typeof(BoxCollider), typeof(Rigidbody), typeof(SplitterSingleCut));

        goCutPlane.GetComponent<Collider>().isTrigger = true;
        Rigidbody bodyCutPlane = goCutPlane.GetComponent<Rigidbody>();
        bodyCutPlane.useGravity = false;
        bodyCutPlane.isKinematic = true;

        Vector3 center = target.position + offset;

        Transform transformCutPlane = goCutPlane.transform;
        transformCutPlane.position = center;
        transformCutPlane.localScale = new Vector3(2f, .01f, 2f);
        transformCutPlane.rotation = rotation;
    }
}
