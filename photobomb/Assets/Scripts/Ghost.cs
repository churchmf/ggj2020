﻿using UnityEngine;

public class Ghost : MonoBehaviour
{
    [SerializeField]
    float _snapThreshold = 2f;

    void OnTriggerStay(Collider other)
    {
        Photo photo = other.GetComponent<Photo>();
        if (photo != null)
        {
            Vector3 delta = other.transform.position - transform.position;
            if (delta.magnitude <= _snapThreshold)
            {
                other.transform.position = transform.position;
                other.transform.rotation = transform.rotation;
                photo.SetSnapped(true);
            }
        }
    }
}
