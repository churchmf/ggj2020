﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnvelopeMenu : MonoBehaviour
{
    [SerializeField]
    MenuManager _menu = null;

    Animator _animator = null;

    void Awake()
    {
        _animator = GetComponent<Animator>();
        _animator.SetTrigger("Active");
        StartCoroutine(_menu.SpawnPhotos());
    }
}
