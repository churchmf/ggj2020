﻿using UnityEngine;

[RequireComponent(typeof(Animator))]
public class Envelope : MonoBehaviour
{
    [SerializeField]
    GameManager _gameManager = null;

    Animator _animator = null;

    void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void OnMouseDown()
    {
        _animator.SetTrigger("Active");
        StartCoroutine(_gameManager.SpawnPhoto());
    }
}
