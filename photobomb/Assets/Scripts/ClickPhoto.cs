﻿using System;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer))]
public class ClickPhoto : MonoBehaviour
{
    [SerializeField]
    bool _scaleToImageAspectRatio = true;

    MeshRenderer _meshRenderer = null;

    Action _onClick = null;

    public void Initialize(Sprite image, Action onClick)
    {
        _onClick = onClick;
        _meshRenderer = GetComponent<MeshRenderer>();
        _meshRenderer.material.SetTexture("_MainTex", image.texture);

        if (_scaleToImageAspectRatio)
        {
            float aspectRatio = image.bounds.size.x / image.bounds.size.y;
            transform.localScale = new Vector3(transform.localScale.x * aspectRatio, transform.localScale.y, transform.localScale.z);
        }
    }

    void OnMouseDown()
    {
        _onClick?.Invoke();
    }
}
