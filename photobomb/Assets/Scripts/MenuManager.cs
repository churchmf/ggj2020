﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

class MenuManager : MonoBehaviour
{
    [SerializeField]
    ClickPhoto _prefab = null;

    [SerializeField]
    CanvasGroup _creditsCanvas = null;

    [SerializeField]
    Sprite _quit = null;
    [SerializeField]
    Sprite _start = null;
    [SerializeField]
    Sprite _credits = null;

    [SerializeField]
    Transform _quitSpawn = null;
    [SerializeField]
    Transform _startSpawn = null;
    [SerializeField]
    Transform _creditsSpawn = null;

    public IEnumerator SpawnPhotos()
    {
        yield return new WaitForSeconds(1f);

        ClickPhoto quit = Instantiate(_prefab, _quitSpawn);
        quit.Initialize(_quit, () =>
        {
#if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
        });

        ClickPhoto start = Instantiate(_prefab, _startSpawn);
        start.Initialize(_start, () =>
        {
            SceneManager.LoadScene("main");
        });

        ClickPhoto credits = Instantiate(_prefab, _creditsSpawn);
        credits.Initialize(_credits, () =>
        {
            StartCoroutine(CreditsFade(2f, 5f));
        });
    }

    IEnumerator CreditsFade(float fadeSpeed, float waitSeconds)
    {
        _creditsCanvas.gameObject.SetActive(true);
        _creditsCanvas.alpha = 0f;
        do
        {
            yield return null;
            _creditsCanvas.alpha += Time.deltaTime * fadeSpeed;
        } while (!Mathf.Approximately(_creditsCanvas.alpha, 1f));

        yield return new WaitForSeconds(waitSeconds);

        do
        {
            yield return null;
            _creditsCanvas.alpha -= Time.deltaTime * fadeSpeed;
        } while (!Mathf.Approximately(_creditsCanvas.alpha, 0f));

        _creditsCanvas.gameObject.SetActive(false);
    }
}
