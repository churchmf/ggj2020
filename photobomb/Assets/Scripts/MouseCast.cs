﻿using UnityEngine;

class MouseCast : MonoBehaviour
{
    [SerializeField]
    LayerMask _targetMask = -1;
    [SerializeField]
    float _selectionHeight = 2f;
    [SerializeField]
    float _followSpeed = 1f;
    [SerializeField]
    float _rayDistance = 20f;
    [SerializeField]
    float _rotationSpeed = 1f;

    Rigidbody _selected = null;
    Vector3 _target;

    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray.origin, ray.direction, out RaycastHit hitInfo, _rayDistance, _targetMask))
            {
                _selected = hitInfo.rigidbody;
            }
        }
        else if (Input.GetMouseButtonUp(0))
        {
            _selected = null;
        }

        if (_selected != null)
        {
            var mouseTarget = new Vector3(Input.mousePosition.x, Input.mousePosition.y, _selectionHeight);
            _target = Camera.main.ScreenToWorldPoint(mouseTarget);

            Vector3 delta = _target - _selected.position;
            _selected.velocity = delta * _followSpeed;
            _selected.rotation = Quaternion.RotateTowards(_selected.rotation, Quaternion.identity, Time.deltaTime * _rotationSpeed);

            _selected.maxAngularVelocity = 2f;
        }
    }

    void OnDrawGizmos()
    {
        if (Application.isPlaying)
        {
            Gizmos.DrawWireSphere(_target, 1f);
        }
    }
}
