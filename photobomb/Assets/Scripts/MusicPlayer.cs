﻿using System;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
class MusicPlayer : MonoBehaviour
{
    [SerializeField]
    AudioClip[] _music = null;

    AudioSource _audioSource = null;
    int _index = 0;

    void Awake()
    {
        DontDestroyOnLoad(transform);
        _audioSource = GetComponent<AudioSource>();

        // Randomly order
        _music = _music.OrderBy(x => Guid.NewGuid()).ToArray();
    }

    void Update()
    {
        if (!_audioSource.isPlaying && _music.Length > 0)
        {
            _audioSource.clip = _music[_index];
            _audioSource.Play();
            _index = _index++ % _music.Length;
        }
    }
}
